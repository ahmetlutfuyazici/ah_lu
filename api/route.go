package api

import "strings"

// Middleware for filter requests
type Middleware struct {
	Name    string
	Handler func(*Context, *Middleware)
	next    *Middleware
}

// Next calling next middleware
func (middleware Middleware) Next(context *Context) {
	if next := middleware.next; next != nil {
		next.Handler(context, next)
	}
}

// list of routers
type Router struct {
	Prefix string
	routers     map[string][]*routeHandler // GET - POST - PUT - DELETE
	middlewares []*Middleware
}

// Use middleware for the router
func (r *Router) Use(middleware *Middleware) {
	// compile middleware
	for index, m := range r.middlewares {
		// replace if middleware have same name
		if m.Name == middleware.Name {
			middleware.next = m.next
			r.middlewares[index] = middleware
			if index > 1 {
				r.middlewares[index-1].next = middleware
			}
			return
		} else if len(r.middlewares) > index+1 {
			m.next = r.middlewares[index+1]
		} else if len(r.middlewares) == index+1 {
			m.next = middleware
		}
	}

	r.middlewares = append(r.middlewares, middleware)
}

// newRouter initializing router
func newRouter() *Router {
	return &Router{routers: map[string][]*routeHandler{
		"GET":    {},
		"PUT":    {},
		"POST":   {},
		"DELETE": {},
	}}
}

type requestHandler func(c *Context)

type routeHandler struct {
	Path   string
	Handle requestHandler
	Public bool
}

// Get register a GET request for path
func (r *Router) Get(path string, handle requestHandler,isPublic bool) {
	r.routers["GET"] = append(r.routers["GET"], newRouteHandler(path, handle,isPublic))
}

// Post register a POST request for path
func (r *Router) Post(path string, handle requestHandler,isPublic bool) {
	r.routers["POST"] = append(r.routers["POST"], newRouteHandler(path, handle,isPublic))
}

// Put register a PUT request for path
func (r *Router) Put(path string, handle requestHandler,isPublic bool) {
	r.routers["PUT"] = append(r.routers["PUT"], newRouteHandler(path, handle,isPublic))
}

// Delete register a DELETE request for path
func (r *Router) Delete(path string, handle requestHandler,isPublic bool) {
	r.routers["DELETE"] = append(r.routers["DELETE"], newRouteHandler(path,handle,isPublic))
}

func newRouteHandler(path string, handle requestHandler,isPublic bool) *routeHandler {
	handler := &routeHandler{
		Path:   "/" + strings.TrimPrefix(path, "/"),
		Handle: handle,
		Public: isPublic,
	}
	return handler
}