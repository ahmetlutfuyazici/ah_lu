package api

import (
	"apidemo/api/models"
	"fmt"
	"github.com/jinzhu/gorm"
	"net/http"
	"strings"
)

type apiMux struct {
	api *Api
}

// ServeHTTP implements http.Handler interface
func (apiMux *apiMux) ServeHTTP(w http.ResponseWriter, req *http.Request) {

	var (
		api = apiMux.api
		relativePath = "/" + strings.Trim(strings.TrimPrefix(req.URL.Path, api.router.Prefix), "/")
		context = api.NewContext(w,req) // current request context
	)

	// Parse Request Form
	req.ParseMultipartForm(2 * 1024 * 1024)
	// release
	defer func() {
		if req.MultipartForm != nil {
			req.MultipartForm.RemoveAll()
		}
	}()

	// get handlers for request method
	handlers := api.router.routers[strings.ToUpper(req.Method)]

	for _, handler := range handlers {
		// if handler & path match
		if relativePath == handler.Path {
			context.RouteHandler = handler
			break
		}
	}

	// Calling first middleware
	for _, middleware := range api.router.middlewares {
		middleware.Handler(context, middleware)
		break
	}
}

// ApiConfig - config for api
type ApiConfig struct {
	DB *gorm.DB
}

type Api struct {
	*ApiConfig
	router *Router
}

func New() *Api {
	api := Api{
		router: newRouter(),
	}
	return &api
}

func (api *Api) SetRoutes() {
	api.router.Post("login",LoginHandler,true)
	api.router.Post("sign-up",SignUpHandler,true)
	api.router.Post("info",InfoHandler,false)
	api.router.Post("forgot",ForgotHandler,true)
	api.router.Post("reset",ResetHandler,true)

	api.router.Get("google",GoogleHandler,true)
}

// AttachTo attach rest api for path
func (api *Api) AttachTo(attactTo string, mux *http.ServeMux) {
	prefix := "/" + strings.Trim(attactTo, "/")
	serveMux := api.NewMux(prefix)
	mux.Handle(prefix, serveMux)                     // => /api
	mux.Handle(fmt.Sprintf("%s/", prefix), serveMux) // => /api/
}

func (api *Api) NewContext(w http.ResponseWriter, r *http.Request) *Context {
	// TODO get from config, global db variable for demo purpose
	return &Context{DB: models.DB, Request: r, Writer: w, Api: api}
}

// NewMux generate http handler for api
func (api *Api) NewMux(prefix string) http.Handler {
	router := api.router
	router.Prefix = prefix

	// initial middleware
	router.Use(&Middleware{
		Name: "cache_handler",
		Handler: func(context *Context, middleware *Middleware) {
			context.Writer.Header().Set("Cache-control", "no-store")
			context.Writer.Header().Set("Pragma", "no-cache")
			if context.RouteHandler != nil {
				context.RouteHandler.Handle(context)
				return
			}
			http.NotFound(context.Writer, context.Request)
		},
	})

	return &apiMux{api: api}
}
