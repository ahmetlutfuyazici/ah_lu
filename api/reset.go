package api

import "apidemo/api/actions"

const msgPasswordSuccessfullyUpdated = "you successfully updated your password"

func ResetHandler(ctx *Context)  {

	// for error handling
	defer ctx.Finalize(msgPasswordSuccessfullyUpdated,"/")

	// context initialization
	if ctx.Error = ctx.init();ctx.Error != nil{
		return
	}

	// user inputs
	args := actions.NewArgs([]actions.Arg{
		{
			Name: "password",
			Required: true,
		},
		{
			Name: "token",
			Required: true,
		},
	})

	// set & validates
	if ctx.Error = args.SetArgs(ctx.Request.Form);ctx.Error != nil {
		return
	}

	ctx.CurrentUser,ctx.Error = args.Reset(ctx.DB)
}
