package models

import (
	"apidemo/api/config"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/qor/validations"
	"log"
	"os"
	"time"
)

var DB *gorm.DB // global db connection
var TestDB *gorm.DB // global test db 

func init()  {
	connect()
	initTestDB()
}

func initTestDB() {
	var err error
	TestDB,err = gorm.Open("sqlite3", "file::memory:?cache=shared")
	//TestDB,err = gorm.Open("sqlite3", "gorm.db")
	if err != nil {
		log.Fatal(err)
	}
	validations.RegisterCallbacks(TestDB)
	TestDB.AutoMigrate(&User{})
}

func connect()  {
	var err error
	// db connection info from config file
	dbConfig := config.Config.DB
	if dbConfig.Adapter == "mysql" {
		DB, err = gorm.Open("mysql", fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?parseTime=True&loc=Local", dbConfig.User, dbConfig.Password, dbConfig.Host, dbConfig.Port, dbConfig.Name))
		// DB = DB.Set("gorm:table_options", "CHARSET=utf8")
	} else if config.Config.DB.Adapter == "postgres" {
		DB, err = gorm.Open("postgres", fmt.Sprintf("postgres://%v:%v@%v/%v?sslmode=disable", dbConfig.User, dbConfig.Password, dbConfig.Host, dbConfig.Name))
	} else if config.Config.DB.Adapter == "sqlite" {
		DB, err = gorm.Open("sqlite3", fmt.Sprintf("%v/%v", os.TempDir(), dbConfig.Name))
	} else {
		log.Fatal(err)
	}

	if err != nil{
		log.Fatal(err)
	}

	DB.LogMode(config.Config.APP.Debug) // debug sql queries
	validations.RegisterCallbacks(DB)

	// run auto migrate if config set
	if config.Config.APP.Migrate {
		autoMigrate()
	}

	// only for mysql
	if dbConfig.Adapter == "mysql" {
		go checkPing()
	}

}

// ping for mysql connection. mysql doest not support connection pool
func checkPing() {
	for {
		time.Sleep(time.Second * 15)
		DB.DB().Ping()
	}
}

// sync table structure
func autoMigrate()  {
	DB.AutoMigrate(&User{})
}