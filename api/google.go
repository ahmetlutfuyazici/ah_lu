package api

import (
	"apidemo/api/config"
	"apidemo/api/actions"
	"apidemo/api/helpers"
	"apidemo/api/models"
	"context"
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"io/ioutil"
)

var oauth2Config *oauth2.Config

func init() {

	oauth2Config = &oauth2.Config{
		ClientID:     config.Config.APP.Googleclientid,
		ClientSecret: config.Config.APP.Googleclientsecret,
		RedirectURL:  config.Config.APP.GoogleAuthUrl(),
		Scopes: []string{
			"https://www.googleapis.com/auth/userinfo.email",
		},
		Endpoint: google.Endpoint,
	}

}

var googleUserInfoURL = "https://www.googleapis.com/oauth2/v3/userinfo"

var msgLoggedSuccessfully = "you have successfully logged"


func GoogleHandler(ctx *Context)  {

	var redirectUrl string

	// TODO test redirection
	defer func() {
		// for error handling
		ctx.Finalize(msgLoggedSuccessfully,redirectUrl)
	}()

	if ctx.Error = ctx.init();ctx.Error != nil{
		return
	}

	// state control from session
	state := helpers.GetState(ctx.Session)
	if state == "" {
		ctx.Error = fmt.Errorf("session state error")
		return
	}

	// compare session & google states
	if state != ctx.Request.URL.Query().Get("state") {
		ctx.Error = fmt.Errorf("bad request")
		return
	}

	// convert code => token
	var token *oauth2.Token
	token, ctx.Error = oauth2Config.Exchange(context.Background(),ctx.Request.URL.Query().Get("code"))
	if ctx.Error != nil {
		return
	}

	// get user info via token
	var userInfo UserInfo
	userInfo,ctx.Error = GetUserInfo(token)
	if ctx.Error != nil {
		return
	}

	// login or create user
	var isNewUser bool
	isNewUser,ctx.Error = userInfo.LoginOrCreate(ctx)
	if ctx.Error == nil {
		if isNewUser {
			redirectUrl = "/info"
		}else{
			redirectUrl = "/profile"
		}
	}
}

func GetUserInfo(token *oauth2.Token) (userinfo UserInfo,err error) {
	userinfo = UserInfo{}
	client := oauth2Config.Client(context.Background(),token)
	resp,err := client.Get(googleUserInfoURL)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	body,err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}
	err = json.Unmarshal(body,&userinfo)
	return
}

// UserInfo google user info model
type UserInfo struct {
	Sub           string `json:"sub"`
	Name          string `json:"name"`
	GivenName     string `json:"given_name"`
	FamilyName    string `json:"family_name"`
	Picture       string `json:"picture"`
	Profile       string `json:"profile"`
	Email         string `json:"email"`
	EmailVerified bool   `json:"email_verified"`
	Gender        string `json:"gender"`
}

// ToUser create user from userInfo
func (userInfo UserInfo) ToUser(tx *gorm.DB) (user models.User,err error)  {
	user = models.User{}
	user.Email = userInfo.Email
	user.FullName = fmt.Sprintf("%s %s",userInfo.Name,userInfo.FamilyName)
	user.FromGoogle = true
	err = tx.Create(&user).Error
	return
}

// LoginOrCreate create a user if the user does not exist and set session
func (userInfo UserInfo) LoginOrCreate(ctx *Context) (isNewUser bool,err error)  {
	user := actions.GetUserByEmail(ctx.DB,userInfo.Email)
	if !user.Authenticated {
		// create user
		user,err = userInfo.ToUser(ctx.DB)
		if err != nil {
			return
		}
		isNewUser = true
	}
	// save user id to session
	ctx.Session.Values["current_user"] = user.ID
	return
}

// GetLoginUrl for Google Sign In
func GetLoginUrl(state string) string  {
	return oauth2Config.AuthCodeURL(state)
}
