package main

import (
	"apidemo/api"
	"apidemo/api/config"
	"apidemo/api/helpers"
	"apidemo/api/models"
	"fmt"
	"github.com/gorilla/sessions"
	"github.com/justinas/nosurf"
	"log"
	"net/http"
)

type Controller struct {
	CurrentUser models.User
	Action string
	writer http.ResponseWriter
	request *http.Request
	Public bool
	Data map[string]interface{} //template data, current user vs
	Tmpl string // template file
	Error error
	Session *sessions.Session
}

// ServeHTTP which implements http.Handler
func (ctx *Controller) ServeHTTP(w http.ResponseWriter, req *http.Request)  {
	if config.Config.APP.Debug {
		reloadTemplates()
	}
	log.Println("request uri :: ",req.RequestURI)

	ctx.writer = w
	ctx.request = req

	defer func() {
		if ctx.hasError() {
			ctx.Session.AddFlash(ctx.Error.Error())
			if err1 := ctx.save();err1 != nil{
				http.Error(ctx.writer, err1.Error(), http.StatusInternalServerError)
				return
			}
			ctx.redirect("/")
			return
		}
	}()

	ctx.init()
	if ctx.hasError() {
		return
	}

	// TODO call action by name using by reflection
	if ctx.checkAction() {
		switch ctx.Action {
		case "login":
			ctx.login()
		case "sign-up":
			ctx.signUp()
		case "info":
			ctx.info()
		case "profile":
			ctx.profile()
		case "forgot":
			ctx.forgot()
		case "reset":
			ctx.reset()
		case "logout":
			ctx.logout()

		default:
			ctx.notFound()
		}
		// if there is an error in actions, there is no need to parse template
		if ctx.hasError() {
			return
		}
		ctx.ExecuteTemplate()
		if ctx.hasError() {
			return
		}
	}else{
		ctx.notFound()
	}
}

func (ctx *Controller) notFound()  {
	http.NotFound(ctx.writer, ctx.request)
}

func (ctx *Controller) hasError() bool  {
	return ctx.Error != nil
}

// init call initSession & initUser
func (ctx *Controller) init() {
	ctx.Data = map[string]interface{}{} // initialize template data

	ctx.initSession()
	if ctx.hasError() {
		return
	}

	ctx.initUser()
	ctx.authGuard()
	if ctx.Error != nil{
		return
	}
	ctx.flashMessages()
}


// Authentication Guard
func (ctx *Controller) authGuard()  {
	if !ctx.Public {
		// if current user is not loggeded in, will redirect to sign in page
		if !ctx.CurrentUser.Authenticated {
			ctx.Error = fmt.Errorf("please login")
			return
		}
	}else{
		// if current user logged in on public pages. will redirect to profile page
		// TODO check for redirection
		if ctx.CurrentUser.Authenticated {
			if !ctx.CurrentUser.InfoRedirected {
				ctx.redirect("/info")
				return
			}
			ctx.redirect("/profile")
			return
		}
	}
}

func (ctx *Controller) redirect(url string)  {
	http.Redirect(ctx.writer, ctx.request,url,http.StatusFound)
}

// Save session
func (ctx *Controller) save() error {
	return ctx.Session.Save(ctx.request,ctx.writer)
}

// initSession initialize session field
func (ctx *Controller) initSession()  {
	ctx.Session, ctx.Error = api.Store.Get(ctx.request,"app-session")
}

// initUser initialize user field
func (ctx *Controller) initUser()  {
	ctx.CurrentUser = api.GetUser(ctx.Session)
}

func (ctx *Controller) ExecuteTemplate()  {
	if ctx.Tmpl != "" {
		if ctx.CurrentUser.Authenticated {
			// if user logged in , pass to template
			ctx.Data["current_user"] = ctx.CurrentUser
		}
		ctx.Data["csrf_token"] = nosurf.Token(ctx.request) // prevent Cross-Site Request Forgery attacks
		ctx.Error = templates.ExecuteTemplate(ctx.writer, ctx.Tmpl, ctx.Data)
	}
}
// flashMessages shows messages to visitor
func (ctx *Controller) flashMessages()  {
	messages := ctx.Session.Flashes()
	ctx.Error = ctx.save()
	if ctx.Error != nil {
		return
	}
	ctx.Data["messages"] = messages
}

// login Login
func (ctx *Controller) login()  {
	ctx.Tmpl = "login.html"
	state := helpers.RandToken(8)
	ctx.Session.Values["state"] = state // random string for oauth
	ctx.Error = ctx.save()
	if ctx.Error != nil{
		return
	}
	ctx.Data["loginUrl"] = api.GetLoginUrl(state)
}

// signUp Sign-Up
func (ctx *Controller) signUp()  {
	ctx.Tmpl = "sign-up.html"
}

// info => Enter Profile Information
func (ctx *Controller) info()  {
	ctx.CurrentUser.InfoRedirected = true
	ctx.Tmpl = "info.html"
}

// profile => Main Profile
func (ctx *Controller) profile()  {
	ctx.Tmpl = "profile.html"
}

// forgot => Forgot Password
func (ctx *Controller) forgot()  {
	ctx.Tmpl = "forgot.html"
}

// reset => Reset Password
func (ctx *Controller) reset()  {
	resetToken := ctx.request.URL.Query().Get("token")
	if resetToken == "" {
		ctx.Error = fmt.Errorf("reset token is empty")
		return
	}
	ctx.Data["reset_token"] = resetToken
	ctx.Tmpl = "reset.html"
}

// logout => Destroy auth session
func (ctx *Controller) logout()  {
	ctx.Session.Values["current_user"] = 0
	ctx.Session.Options.MaxAge = -1
	ctx.Error = ctx.save()
	if ctx.Error != nil {
		return
	}
	ctx.redirect("/")
}


// checkAction check action is exist
func (ctx *Controller) checkAction() bool {
	for _, action := range actions {
		if action == ctx.Action {
			return true
		}
	}
	return false
}
