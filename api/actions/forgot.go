package actions

import (
	"apidemo/api/helpers"
	"apidemo/api/models"
	"github.com/jinzhu/gorm"
	"time"
)

// Forgot Helper
func (args Args) Forgot(tx *gorm.DB) (user models.User,err error)  {
	user,err = args.getUser(tx)
	if err != nil {return}
	token := helpers.RandToken(20)
	user.RecoverToken = token
	tokenExpiry := time.Now().Add(time.Minute * 15)
	user.RecoverTokenExpiry = &tokenExpiry // expires in 15 minutes
	err = user.Save(tx)
	if err != nil {
		return
	}
	// send email
	err = sendResetEmail(user)
	return // send email
}
