package actions

import (
	"apidemo/api/models"
	"github.com/jinzhu/gorm"
)

// Login with user inputs
func (args Args) Login(tx *gorm.DB) (user models.User,err error) {
	user,err = args.getUser(tx)
	if err != nil {return}
	err = user.ComparePassword(args.Get("password"))
	return
}