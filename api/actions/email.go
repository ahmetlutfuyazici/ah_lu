package actions

import (
	"apidemo/api/config"
	"apidemo/api/models"
	"bytes"
	"crypto/tls"
	"fmt"
	"gopkg.in/mail.v2"
	"html/template"
)

// compile email templates
var templates = template.Must(template.ParseGlob("templates/emails/*"))

type Mailer struct {
	mail.SendCloser
	*mail.Dialer
}
// AppMailer global email sender variable
var AppMailer Mailer

func init()  {
	err := initMailer()
	if err != nil {
		panic(err)
	}
}

func initMailer() error  {
	AppMailer = Mailer{}
	AppMailer.Dialer = mail.NewDialer(config.Config.SMTP.Host, config.Config.SMTP.Port, config.Config.SMTP.User, config.Config.SMTP.Password)
	AppMailer.Dialer.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	var err error
	AppMailer.SendCloser, err = AppMailer.Dial()
	return err
}

func getEmailBody(data map[string]string,filename string) (string,error)  {
	var tpl bytes.Buffer
	err := templates.ExecuteTemplate(&tpl,filename,data)
	if err != nil {
		return "", err
	}
	return tpl.String(),nil
}

// sendResetEmail sending email for recovering user password
func sendResetEmail(user models.User) error  {
	err := initMailer()
	if err != nil {
		return err
	}
	m := mail.NewMessage()
	m.SetHeader("From",config.Config.SMTP.User)
	m.SetAddressHeader("To", user.Email, user.FullName)
	m.SetHeader("Subject", "Password Recovery")
	emailBody,err := getEmailBody(user.ResetEmailData(),"forgot.html")
	if err != nil {
		return err
	}
	m.SetBody("text/html",emailBody)
	if err := mail.Send(AppMailer.SendCloser,m);err != nil {
		return fmt.Errorf("some errors occurred while sending email %s",err)
	}
	m.Reset()
	return nil
}
