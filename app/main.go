package main

import (
	"apidemo/api"
	"apidemo/api/config"
	"fmt"
	"github.com/justinas/nosurf"
	"log"
	"net/http"
)


func main() {

	// if debug is true, show log lines
	if config.Config.APP.Debug {
		log.SetFlags(log.LstdFlags | log.Lshortfile)
	}

	// Initalize an HTTP ServeMux
	mux := http.NewServeMux()

	restApi := api.New()
	restApi.SetRoutes()
	restApi.AttachTo("/api",mux)

	// client
	log.Println("client id")
	log.Println("client secret")
	log.Println(config.Config.APP.Googleclientid)
	log.Println(config.Config.APP.Googleclientsecret)

	initFrontendHandlers(mux)

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d",config.Config.APP.Port),panicRecovery(nosurf.New(mux))))
}

// panicRecovery
func panicRecovery(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, rq *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				http.Error(rw, fmt.Errorf("error %s",err).Error(), http.StatusInternalServerError)
			}
		}()

		handler.ServeHTTP(rw, rq)
	})
}