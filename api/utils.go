package api

import (
	"apidemo/api/models"
	"github.com/gorilla/sessions"
)

func GetUser(s *sessions.Session) models.User {
	var userId uint
	if val,ok := s.Values["current_user"];ok{
		userId,ok = val.(uint)
		if !ok || userId == 0 {
			return models.User{}
		}
		return models.GetUserById(userId)
	}
	return models.User{}
}

