package actions

import (
	"apidemo/api/models"
	"testing"
)

func TestArgs_CreateUser(t *testing.T) {

	initTestUsers()

	type fields struct {
		inputs []Arg
	}

	tests := []struct {
		name     string
		fields   fields
		wantErr  bool
	}{
		// create user with the existing email address
		{
			name: "user1",
			fields: fields{inputs: []Arg{
				{
					Name: "email",
					Value: "abc@gmail.com",
					Required: true,
				},
				{
					Name: "password",
					Value: "12345678",
					Required: true,
				},
			},
			},
			wantErr: true,
		},
		// try to create user with invalid email format
		{
			name: "user2",
			fields: fields{inputs: []Arg{
				{
					Name: "email",
					Value: "abcgmail.com",
					Required: true,
				},
				{
					Name: "password",
					Value: "12345678",
					Required: true,
				},
			},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			args := Args{
				inputs: tt.fields.inputs,
			}
			_, err := args.CreateUser(models.TestDB)
			if (err != nil) != tt.wantErr {
				t.Errorf("Args.CreateUser() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

		})
	}
}
