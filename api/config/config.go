package config

import (
	"fmt"
	"github.com/jinzhu/configor"
)

var Config = struct {
	APP  APPConfig
	DB   DBConfig
	SMTP SMTPConfig
}{}

type SMTPConfig struct {
	Host     string
	Port     int
	User     string
	Password string
}

type APPConfig struct {
	Host string `default:"http://localhost" env:"HOST"`
	Port uint `default:"7000" env:"PORT"`
	Debug bool `default:"false" env:"DEBUG"`
	Googleclientid string `env:"GOOGLE_CLIENT_ID"`
	Googleclientsecret string `env:"GOOGLE_CLIENT_SECRET"`
	Migrate bool `default:"false" env:"MIGRATE"` // if true, db will sync table structure
}

func (app APPConfig) GoogleAuthUrl() string  {
	// for godemo.pushecommerce.com
	return fmt.Sprintf("%s/api/google",app.Host)
}

type DBConfig struct {
	Name     string `env:"DBName" default:"demo"`
	Adapter  string `env:"DBAdapter" default:"mysql"`
	Host     string `env:"DBHost" default:"localhost"`
	Port     string `env:"DBPort" default:"3306"`
	User     string `env:"DBUser"`
	Password string `env:"DBPassword"`
}

func init() {
	if err := configor.Load(&Config, "config/database.yml", "config/smtp.yml","config/application.yml"); err != nil {
		panic(err)
	}
}
