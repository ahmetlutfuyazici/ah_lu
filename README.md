# Quick Start

```sh
# Rename & setup all config files which located at app/config

# get dependencies 
 go mod vendor
# go to app folder & run
 cd app
 go run .
```
