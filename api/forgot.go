package api

import "apidemo/api/actions"

const msgCheckYourEmail = "Check your email for a link to reset your password. If it doesn’t appear within a few minutes, check your spam folder."

func ForgotHandler(ctx *Context)  {

	// for error handling
	defer ctx.Finalize(msgCheckYourEmail,"/")

	// context initialization
	if ctx.Error = ctx.init();ctx.Error != nil{
		return
	}

	// user inputs
	args := actions.NewArgs([]actions.Arg{
		{
			Name: "email",
			Required: true,
		},
	})

	// set & validates
	if ctx.Error = args.SetArgs(ctx.Request.Form);ctx.Error != nil {
		return
	}

	ctx.CurrentUser,ctx.Error = args.Forgot(ctx.DB)
}