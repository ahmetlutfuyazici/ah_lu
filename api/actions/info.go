package actions

import (
	"apidemo/api/helpers"
	"apidemo/api/models"
	"github.com/jinzhu/gorm"
)

// Info update profile
func (args Args) Info(tx *gorm.DB,user models.User) (err error) {
	email := args.Get("email")
	// update only input fields, not all fields
	var params = map[string]interface{}{}
	// google users cannot update their email
	if !user.FromGoogle && email != ""  && user.Email != email { // Is there a user with this email?
		if !helpers.IsEmailValid(email) {
			return errorInvalidEmailAddress
		}
		if models.IsUserExist(tx,email) {
			return errorAccountAlreadExist
		}
		params["email"] = email
	}
	if !user.InfoRedirected {
		params["info_redirected"] = true
	}
	params["full_name"] = args.Get("full_name")
	params["address"] = args.Get("address")
	params["telephone"] = args.Get("telephone")
	return tx.Model(&user).Updates(params).Error
}
