package main

import (
	"html/template"
	"net/http"
	"os"
	"strings"
)

// compile templates
var templates = template.Must(template.ParseGlob("templates/views/*"))

// reloadTemplates if debug mode true, parse again on every request
func reloadTemplates()  {
	templates = template.Must(template.ParseGlob("templates/views/*"))
}

// action list
var actions []string

// newController initialize controller for client request
func newController(action string, isPublic bool) *Controller {
	actions = append(actions, action)
	return &Controller{Action: action, Public: isPublic}
}

func initFrontendHandlers(mux *http.ServeMux) {
	mux.Handle("/", newController("login", true))
	mux.Handle("/sign-up", newController("sign-up", true))
	mux.Handle("/info", newController("info", false))
	mux.Handle("/profile", newController("profile", false))
	mux.Handle("/forgot", newController("forgot", true))
	mux.Handle("/reset", newController("reset", true))
	mux.Handle("/logout", newController("logout", false))

	mux.Handle("/static/",fileserver())
	mux.HandleFunc("/favicon.ico", faviconHandler)
}

func faviconHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "static/img/favicon.ico")
}

// fileserver that disabled file listing
func fileserver() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		p := strings.TrimLeft(r.URL.Path,"/")
		if f, err := os.Stat(p); err == nil && !f.IsDir() {
			http.ServeFile(w, r, p)
			return
		}

		http.NotFound(w, r)
	})
}
