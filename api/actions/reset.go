package actions

import (
	"apidemo/api/models"
	"fmt"
	"github.com/jinzhu/gorm"
	"time"
)

func (args Args) Reset(tx *gorm.DB) (user models.User,err error)  {
	user,err = args.getUserByToken(tx)
	if err != nil {return}
	// reset user token
	defer func() {
		//user.RecoverTokenExpiry = time.Time{} // user pointer due to this error " Incorrect datetime value: '0000-00-00' for column 'recover_token_expiry' at row 1 "
		user.RecoverToken = "" // reset token
		defer func() { // for saving user
			if err == nil {
				err = user.Save(tx)
			}
		}()
		if err == nil{
			user.Password = args.Get("password")
			err = user.HashPassword()
		}
	}()
	if args.Get("token") != user.RecoverToken {
		err = fmt.Errorf("reset token is invalid")
		return
	}
	if user.RecoverTokenExpiry == nil {
		err = fmt.Errorf("bad request")
		return
	}
	if user.RecoverTokenExpiry.Before(time.Now()) {
		err = fmt.Errorf("the password reset link has expired")
		return
	}
	return
}