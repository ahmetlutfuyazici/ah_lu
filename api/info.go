package api

import "apidemo/api/actions"


const msgUpdatedSuccessfully = "Your profile updated successfully"

// InfoHanler for saving user profile
func InfoHandler(ctx *Context)  {


	// for error handling
	defer ctx.Finalize(msgUpdatedSuccessfully,"/")

	// context initialization
	if ctx.Error = ctx.init();ctx.Error != nil{
		return
	}

	// user inputs
	args := actions.NewArgs([]actions.Arg{
		{
			Name: "email",
		},
		{
			Name: "full_name",
		},
		{
			Name: "address",
		},
		{
			Name: "telephone",
		},
	})

	// set & validates
	if ctx.Error = args.SetArgs(ctx.Request.Form);ctx.Error != nil {
		return
	}
	// update user info
	ctx.Error = args.Info(ctx.DB,ctx.CurrentUser)
}
