package api

import (
	"encoding/gob"
	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
)

// TODO cookie store with sample key. Pass it via an environmental variable
var Store = sessions.NewCookieStore([]byte("sample-key"))

func init()  {
	Store.Options = &sessions.Options{
		MaxAge: 86400 * 30,
		HttpOnly: true,
		Path: "/",
	}
	gob.Register(securecookie.MultiError{})
}
