package api

import "apidemo/api/actions"

// LoginHandler handler for login
func LoginHandler(ctx *Context) {
	// for error handling
	defer ctx.Finalize(msgLoggedSuccessfully,"/profile")

	// context initialization
	if ctx.Error = ctx.init();ctx.Error != nil{
		return
	}

	// user inputs
	args := actions.NewArgs([]actions.Arg{
		{
			Name: "email",
			Required: true,
		},
		{
			Name: "password",
			Required: true,
		},
	})

	// set & validates
	if ctx.Error = args.SetArgs(ctx.Request.Form);ctx.Error != nil {
		return
	}

	ctx.CurrentUser,ctx.Error = args.Login(ctx.DB)
	// set session
	if ctx.Error == nil {
		ctx.Session.Values["current_user"] = ctx.CurrentUser.ID
	}
}
