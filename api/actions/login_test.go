package actions

import (
	"apidemo/api/models"
	"log"
	"testing"
	"time"
)

var users []models.User



func initTestUsers()  {

	var time15minutesLater = time.Now()

	users = []models.User{
		{
			Email: "abc@gmail.com",
			Password: "12345678",
			FromGoogle: true,
			RecoverToken: "abcde",
			RecoverTokenExpiry: &time15minutesLater,
		},
		{
			Email: "abc@xyz.com",
			Password: "12345678",
		},
	}
	for i:= 0; i < len(users);i++ {
		log.Println(users[i])
		models.TestDB.Create(&users[i])
	}
}

func TestArgs_Login(t *testing.T) {
	initTestUsers()
	type fields struct {
		inputs []Arg
	}

	tests := []struct {
		name     string
		fields   fields
		wantUser models.User
		wantErr  bool
	}{
		// usual login
		{
			name: "user1",
			fields: fields{inputs: []Arg{
				{
					Name: "email",
					Required: true,
					Value: "abc@gmail.com",
				},
				{
					Name: "password",
					Required: true,
					Value: "12345678",
				},
			}},
			wantUser: users[0],
			wantErr: false,
		},
		// wrong password
		{
			name: "user2",
			fields: fields{inputs: []Arg{
				{
					Name: "email",
					Required: true,
					Value: "abc@xyz.com",
				},
				{
					Name: "password",
					Required: true,
					Value: "12345---",
				},
			}},
			wantErr: true,
			wantUser: users[1],
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			args := Args{
				inputs: tt.fields.inputs,
			}
			gotUser, err := args.Login(models.TestDB)
			if (err != nil) != tt.wantErr {
				t.Errorf("Args.Login() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotUser.Password != tt.wantUser.Password {
				t.Errorf("Args.Login() = %v, want %v", gotUser, tt.wantUser)
			}
		})
	}
}
