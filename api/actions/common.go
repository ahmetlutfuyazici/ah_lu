package actions

import (
	"apidemo/api/helpers"
	"apidemo/api/models"
	"fmt"
	"github.com/jinzhu/gorm"
	"net/url"
	"strings"
)

var (
	// common errors
	errorAccountAlreadExist = fmt.Errorf("an account with this email already exists")
	errorInvalidCredentials = fmt.Errorf("invalid credentials") // this is a generic error for invalid email & password inputs to avoid brute force login attacks
	errorEmailDoesNotExist = fmt.Errorf("this email does not exist")
	errorInvalidEmailAddress = fmt.Errorf("invalid email address")
)

// Args for user inputs
type Args struct {
	inputs []Arg
}

// SetArgs set args & validates
func (args Args) SetArgs(values url.Values) error {
	for i:= 0; i < len(args.inputs); i++{
		currentArg := args.inputs[i]
		currentArg.Value = strings.TrimSpace(values.Get(currentArg.Name))
		if !currentArg.isValid() {
			if currentArg.isEmail() && !currentArg.isEmpty() {
				return errorInvalidEmailAddress
			}
			return currentArg.blankError()
		}
		args.inputs[i] = currentArg
	}
	return nil
}

// Get get arg value with key
func (args Args) Get(key string) string  {
	for _, input := range args.inputs {
		if input.Name == key {
			return input.Value
		}
	}
	return ""
}

// Get User via user email input
func (args Args) getUser(tx *gorm.DB) (user models.User,err error) {
	user = GetUserByEmail(tx,args.Get("email"))
	if !user.Authenticated {
		err = errorEmailDoesNotExist // must be generic error to avoid brute force login attack
	}
	return
}
// getUserByToken get user via reset token
func (args Args) getUserByToken(tx *gorm.DB) (user models.User,err error) {
	user = models.User{}
	tx.Where("recover_token = ?",args.Get("token")).First(&user)
	if user.ID == 0 {
		err = fmt.Errorf("reset token is invalid")
	}
	return
}

// NewArgs initialize user inputs
func NewArgs(params []Arg) Args  {
	args := Args{}
	args.inputs = params
	return args
}

type Arg struct {
	Name string
	Required bool
	Value string
}

// isEmpty checking if the user input is empty
func (arg Arg) isEmpty() bool {
	return arg.Value == ""
}
// blankError dynamic blank error
func (arg Arg) blankError() error  {
	return fmt.Errorf("%s can't be blank",arg.Name)
}

// isEmail checking if the user input is email
func (arg Arg) isEmail() bool  {
	return arg.Name == "email"
}

func (arg Arg) isValid() bool  {

	if !arg.Required {
		return true
	}
	if arg.isEmail() {
		return helpers.IsEmailValid(arg.Value)
	}

	return !arg.isEmpty()
}

func GetUserByEmail(tx *gorm.DB,email string) models.User  {
	user := models.User{}
	tx.Where("email = ?",email).First(&user)
	user.Authenticated = user.ID > 0 // if it is true, it is exist
	return user
}

