package api

import "apidemo/api/actions"

const msgSuccessFullyRegistered = "You have successfully registered"

// SignUpHandler handler for login
func SignUpHandler(ctx *Context) {
	// for error handling
	defer ctx.Finalize(msgSuccessFullyRegistered,"/info")

	// context initialization
	if ctx.Error = ctx.init();ctx.Error != nil{
		return
	}

	// user inputs
	args := actions.NewArgs([]actions.Arg{
		{
			Name: "email",
			Required: true,
		},
		{
			Name: "password",
			Required: true,
		},
	})

	// set & validates
	if ctx.Error = args.SetArgs(ctx.Request.Form);ctx.Error != nil {
		return
	}

	//// try to create use via args
	ctx.CurrentUser,ctx.Error = args.CreateUser(ctx.DB)
	if ctx.Error == nil {
		ctx.Session.Values["current_user"] = ctx.CurrentUser.ID
	}
}

