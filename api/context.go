package api

import (
	"apidemo/api/actions"
	"apidemo/api/models"
	"fmt"
	"github.com/gorilla/sessions"
	"github.com/jinzhu/gorm"
	"net/http"
	"strings"
)

type Context struct {
	Request      *http.Request
	Writer       http.ResponseWriter
	CurrentUser  models.User
	DB           *gorm.DB
	Api          *Api
	RouteHandler *routeHandler
	Session      *sessions.Session
	Error        error
	Args         actions.Args
}

// Save session
func (ctx *Context) Save() error {
	return ctx.Session.Save(ctx.Request,ctx.Writer)
}

func (ctx *Context) Redirect(url string)  {
	http.Redirect(ctx.Writer,ctx.Request,url,http.StatusFound)
}

// Finalize for error handling and redirection
func (ctx *Context) Finalize(msg string,redirectUrl string)  {
	if ctx.Error != nil {
		ctx.Session.AddFlash(ctx.Error.Error())
	}else{
		ctx.Session.AddFlash(msg)
	}
	if err1 := ctx.Save();err1 != nil{
		http.Error(ctx.Writer, err1.Error(), http.StatusInternalServerError)
		return
	}
	if ctx.Error == nil {
		ctx.Redirect(redirectUrl)
		return
	}

	userRedirection := strings.TrimSpace(ctx.Request.Form.Get("redir"))
	if userRedirection == ""{
		ctx.Redirect(ctx.Request.Referer())
		return
	}
	ctx.Redirect(userRedirection)
}

func (ctx Context) isPublic() bool {
	return ctx.RouteHandler.Public
}

func (ctx *Context) init() (err error)  {
	if ctx.IsJsonRequest() {
		// json decode
		// TODO for single page application
		err = fmt.Errorf("invalid request format")
		return
	}
	if err = ctx.Request.ParseForm();err != nil {
		return
	}

	ctx.Session,err = Store.Get(ctx.Request,"app-session")
	if err != nil {
		return
	}

	if ctx.isPublic() {
		ctx.CurrentUser = models.User{}
	}else{
		ctx.CurrentUser = GetUser(ctx.Session)
		if !ctx.CurrentUser.Authenticated {
			err = fmt.Errorf("forbidden")
			return
		}
	}

	return nil
}

// IsJsonRequest check for request header
func (ctx *Context) IsJsonRequest() bool {
	return strings.Contains(ctx.Request.Header.Get("Content-Type"),"application/json")
}