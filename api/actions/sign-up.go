package actions

import (
	"apidemo/api/models"
	"github.com/jinzhu/gorm"
)

// CreateUser create user with user input
func (args Args) CreateUser(tx *gorm.DB) (user models.User,err error)  {
	user = models.User{}
	user.Email = args.Get("email")
	user.Password = args.Get("password")
	if models.IsUserExist(tx,user.Email) {
		return models.User{}, errorAccountAlreadExist
	}
	if err := tx.Create(&user).Error;err != nil {
		return models.User{},err
	}
	user.Authenticated = true
	return user,nil
}