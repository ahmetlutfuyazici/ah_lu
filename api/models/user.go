package models

import (
	"apidemo/api/helpers"
	"fmt"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
	"time"
)

var errorPasswordIncorrect = fmt.Errorf("password incorrect")

type User struct {
	ID         uint `gorm:"primary_key"`
	CreatedAt  time.Time
	UpdatedAt  time.Time
	FullName   string // Full Name
	Email      string `gorm:"unique;not null"`
	Password   string //hashed - empty if used google sign in
	Address    string `sql:"type:text"`
	Telephone  string
	FromGoogle bool   // true if used google sign in
	InfoRedirected bool // if it is false, user will be redirected /info url
	Authenticated bool `gorm:"-"` // for helpers
	// Recover
	RecoverToken       string
	RecoverTokenExpiry *time.Time
}

func (u *User) BeforeCreate() error {
	if !u.FromGoogle {
		return u.HashPassword()
	}else{
		u.Password = ""
	}
	return nil
}

// Validate User Data => added email validation
func (u User) Validate(tx *gorm.DB) {
	if !helpers.IsEmailValid(u.Email) {
		tx.AddError(fmt.Errorf("invalid email format"))
	}
}

// HashPassword : hashing the password
func (u *User) HashPassword() (err error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		return
	}
	u.Password = string(hash)
	return nil
}

// ComparePassword : compare the password
func (u *User) ComparePassword(password string) error {
	if err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password));err != nil {
		return errorPasswordIncorrect // must be generic error to avoid brute force login attack
	}
	return nil
}

func (u User) ResetEmailData() map[string]string  {
	var data = map[string]string{}
	data["token"] = u.RecoverToken
	data["validity"] = u.RecoverTokenExpiry.Format("Monday, Jan 2, 2006, 15:04:05")
	return data
}

func (u User) Save(tx *gorm.DB) error  {
	return tx.Save(&u).Error
}

// GetUserById get the user by given id
func GetUserById(id uint) User {
	user := User{}
	DB.First(&user, id)
	if user.ID == 0 { // if user is not exist
		return User{}
	}
	user.Authenticated = true
	return user
}

// IsUserExist user is exist?
func IsUserExist(tx *gorm.DB, email string) bool {
	user := User{}
	tx.Where("email = ?", email).First(&user)
	return user.ID > 0
}


