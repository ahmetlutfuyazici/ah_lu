package actions

import (
	"apidemo/api/models"
	"testing"
)

func TestArgs_Info(t *testing.T) {

	initTestUsers()

	type fields struct {
		inputs []Arg
	}
	type args struct {
		user models.User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// try to update with existing email address
		{
			name: "user1",
			fields: fields{inputs: []Arg{
				{
					Name: "email",
					Value: "abc@gmail.com",
				},
			}},
			args: args{user: users[1]},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			args := Args{
				inputs: tt.fields.inputs,
			}
			if err := args.Info(models.TestDB, tt.args.user); (err != nil) != tt.wantErr {
				t.Errorf("Args.Info() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
